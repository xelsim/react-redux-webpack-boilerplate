const VisualizerPlugin = require('webpack-visualizer-plugin');
const webpack = require('webpack');
const path = require('path');

module.exports = (env) => {
    return {
        devtool: env !== 'production' ? 'source-map' : false,
        devServer: {
            disableHostCheck: true,
            port: 8088,
        },
        plugins: [new webpack.HotModuleReplacementPlugin(), new VisualizerPlugin({ filename: `../stats.html` })],
        entry: {
            project: ['@babel/polyfill', 'react-hot-loader/patch', './src/index'],
        },
        output: {
            path: path.resolve(__dirname, './web'),
            filename: '[name].js',
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    loaders: ['style-loader', 'css-loader?modules&importLoaders=1&camelCase=only', 'sass-loader'],
                },
                { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
                { test: /\.(jpe?g|png|gif|svg)$/i, loader: 'url-loader?name=images/[name].[ext]' },
                {
                    test: /\.css$/,

                    use: ['style-loader', 'css-loader'],
                },
            ],
        },
    };
};
