# react-redux-webpack-boilerplate

## Init project
```
npm install
```

---

## Launch dev server, watcher and live reload

Make sur that the port 8088 is available (Or change it on webpack.config.js)

```
npm run dev:serve
```

You can access to the website by this address http://localhost:8088

## ESLint

```
npm run dev:eslint
```
## Test

```
npm run test
```