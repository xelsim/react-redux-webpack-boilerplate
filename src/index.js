import React from 'react';
import ReactDOM from 'react-dom';
import '@babel/polyfill';
import Root from './containers/Root';
import { createBrowserHistory } from 'history';
import rootReducer from './reducers';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { routerMiddleware, ConnectedRouter } from 'connected-react-router';

const history = createBrowserHistory();

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer(history), composeEnhancer(applyMiddleware(routerMiddleware(history))));

const render = () => {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Root />
            </ConnectedRouter>
        </Provider>,

        document.getElementById('root')
    );
};

render();

if (module.hot) {
    module.hot.accept('./containers/Root', () => {
        render();
    });

    module.hot.accept('./reducers', () => {
        store.replaceReducer(rootReducer(history));
    });
}
