import { connect } from 'react-redux';
import Root from '../components/Root';
import { withRouter } from 'react-router-dom';

export default withRouter(connect((state) => ({}))(Root));
